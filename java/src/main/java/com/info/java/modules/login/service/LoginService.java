package com.info.java.modules.login.service;

import com.info.java.modules.login.dao.LoginDAO;
import com.info.java.modules.login.jpa.Role;
import com.info.java.modules.user.jpa.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LoginService {
    @Autowired
    LoginDAO loginDAO;

    public int sampleDBTest() {
        return loginDAO.sampleDbTest();
    }

    public Role getRole(String role) {
        return loginDAO.getRole(role);
    }

    public User login(String username, String password) {
        return loginDAO.login(username, password);
    }
}
