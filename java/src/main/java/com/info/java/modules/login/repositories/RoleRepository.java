package com.info.java.modules.login.repositories;

import com.info.java.modules.login.jpa.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends CrudRepository<Role, Integer> {
    Role findRolesByRole(String name);
}
