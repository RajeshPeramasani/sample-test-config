package com.info.java.modules.login.controller;

import com.info.java.modules.login.jpa.Role;
import com.info.java.modules.login.service.LoginService;
import com.info.java.modules.login.uivo.RoleUIVO;
import com.info.java.modules.user.jpa.User;
import com.info.java.modules.user.uivo.UserUIVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;

    @RequestMapping(value = "/sampleDBTest", method = RequestMethod.GET)
    public
    @ResponseBody
    int sampleDBTest() {
        return loginService.sampleDBTest();
    }

    @RequestMapping(value = "/role", method = RequestMethod.GET)
    public
    @ResponseBody
    Role getRole(@RequestParam("roleName") String role) {
        return loginService.getRole(role);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public
    @ResponseBody
    UserUIVO login(HttpServletRequest request, @RequestBody UserUIVO uivo) throws Exception {
        String username = uivo.getUsername();
        String password = uivo.getPassword();
        User response = loginService.login(username, password);

        if (response == null) {
            throw new Exception("Invalid username or password");

        } else if (response.getUsername().equals(username) && response.getPassword().equals(password)) {
            UserUIVO user = new UserUIVO();
            user.setId(response.getId());
            user.setUsername(response.getUsername());
            user.setName(response.getName());
            user.setEmailId(response.getEmailId());
            user.setPhoneNumber(response.getPhoneNumber());
            user.setActive(response.getActive());
            user.setRoleId(response.getRoleId());
            return user;
        }
        throw new Exception("Invalid username or password");
    }
}
