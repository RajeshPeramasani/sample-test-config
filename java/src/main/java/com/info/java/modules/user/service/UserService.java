package com.info.java.modules.user.service;

import com.info.java.modules.user.dao.UserDAO;
import com.info.java.modules.user.jpa.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserService {
    @Autowired
    UserDAO userDAO;

    public User getUser(String role){
        return userDAO.getUser(role);
    }
}
