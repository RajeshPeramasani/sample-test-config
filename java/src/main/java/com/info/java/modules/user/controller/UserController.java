package com.info.java.modules.user.controller;

import com.info.java.modules.user.jpa.User;
import com.info.java.modules.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public
    @ResponseBody
    User getUser(@RequestParam("username") String username) {
        return userService.getUser(username);
    }
}
