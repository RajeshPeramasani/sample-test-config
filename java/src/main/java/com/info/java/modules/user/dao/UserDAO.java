package com.info.java.modules.user.dao;

import com.info.java.modules.user.jpa.User;
import com.info.java.modules.user.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
public class UserDAO {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("dataSource")
    private DataSource dataSource;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    UserRepository userRepository;

    public User getUser(String username) {
        try {
            return userRepository.getUserByUsername(username);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }
}
