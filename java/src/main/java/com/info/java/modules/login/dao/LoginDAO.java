package com.info.java.modules.login.dao;

import com.info.java.modules.login.jpa.Role;
import com.info.java.modules.login.repositories.RoleRepository;
import com.info.java.modules.user.jpa.User;
import com.info.java.modules.user.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

@Component
public class LoginDAO {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("dataSource")
    private DataSource dataSource;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserRepository userRepository;

    public int sampleDbTest() {
        int result = jdbcTemplate.queryForObject(
                "SELECT COUNT(*) FROM Roles", Integer.class);
        log.debug("count" + result);
        return result;
    }

    public Role getRole(String roleName) {
        try {
            return roleRepository.findRolesByRole(roleName);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    public User login(String username, String password) {
        try {
            User response = userRepository.getUserByUsernameAndPassword(username, password);
            return response;

        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }
}
