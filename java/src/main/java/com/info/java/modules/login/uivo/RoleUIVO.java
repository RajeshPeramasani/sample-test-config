package com.info.java.modules.login.uivo;

public class RoleUIVO {
    private Integer id;
    private String role;
    private Boolean active;

    public RoleUIVO() {
    }

    public RoleUIVO(Integer id, String role, Boolean active) {
        this.id = id;
        this.role = role;
        this.active = active;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
