package com.info.java.modules.user.jpa;


import javax.persistence.*;

@Entity
@Table(name = "Users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "Id")
    private Integer id;

    @Column(name = "username")
    private String username;

    @Column(name = "fullname")
    private String name;

    @Column(name = "password")
    private String password;

    @Column(name = "roleid")
    private Integer roleId;

    @Column(name = "phonenumber")
    private String phoneNumber;

    @Column(name = "emailid")
    private String emailId;

    @Column(name = "active")
    private Boolean active;


    public User() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", fullName=" + name + '\'' +
                ", emailId=" + emailId + '\'' +
                ", phoneNumber=" + phoneNumber + '\'' +
                ", roleId=" + roleId + '\'' +
                ", active=" + active +
                '}';
    }
}
