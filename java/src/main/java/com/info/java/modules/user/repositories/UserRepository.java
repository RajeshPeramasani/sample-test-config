package com.info.java.modules.user.repositories;

import com.info.java.modules.user.jpa.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    User getUserByUsername(String username);

    User getUserByUsernameAndPassword(String username, String password);
}
