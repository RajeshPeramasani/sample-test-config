import React from 'react';

class DownloadUtils extends React.Component {

  static JSONToCSVConvertor(JSONData, fileName) { //this method is only for dowloading of tweets this canot be generalized as this is not a regular json format

    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

    var CSV = '';
    //Set Report title in first row or line

    CSV += "TweetID,Tweet,Sentiment,TagCount" + '\r\n';

    //This condition will generate the Label/Header


    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
      var row = "";

      //2nd loop will extract each column and convert it in string comma-seprated
      // for (var index in arrData[i]) {
      row += '"' + arrData[i].TweetID.S + '",';
      row += '"' + arrData[i].TweetText.S + '",';
      row += '"' + arrData[i].FinalSentiment.N + '",';
      row += '"' + arrData[i].TagCount.N + '",';

      //}

      row.slice(0, row.length - 1);

      //add a line break after each row
      CSV += row + '\r\n';
    }

    if (CSV == '') {
      alert("Invalid data");
      return;
    }

    //Generate a file name
    var fileName = fileName;
    fileName.replace(/ /g, "_");

    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension

    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");
    link.href = uri;

    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";

    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }


  render() {
    return (
      <div>
      </div>
    );
  }
}

export default DownloadUtils;
