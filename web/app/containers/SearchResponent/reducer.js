import {fromJS} from 'immutable';
import {SEARCH} from './constants';

export const initialState = fromJS({
  name: '',
  listItems: [],
});

function searchReducer(state = initialState, action) {
  switch (action.type) {
    case  SEARCH:

      return {
        ...state,
        name: action.name,
        listItems: [
          ...state.listItems,
          {
            id: state.listItems.length + 1,
            name: action.name
          }
        ]
      };
    default:
      return state
  }

}
export default searchReducer;
