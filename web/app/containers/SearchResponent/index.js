import React from 'react'
import {Button, Input, Dimmer, Loader} from 'semantic-ui-react';
import {compose} from 'redux';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';

import {search} from '/actions';
import reducer from './reducer';
import './search.css'
import {Login} from "../Login";
import {mapDispatchToProps} from "../HomePage";

export class SearchResponent extends React.Component{
  state={
    name:'',
    nameError:false
  };


  onSearchNameChange = (event, {value}) => {
    this.setState({name: value, nameError: false});
  };

  render(){
    return(
      <div className='search-container'>
        <div className='search-field'>
          <Input ref='searchInput' icon='search' placeholder="Search Name" onChange={this.onSearchNameChange} />
        </div>
        <div>
          <Button
            primary
            content='Search'
            onClick={() => this.props.search(this.state.name)}
          />
        </div>

      </div>
    )
  }

}
SearchResponent.propTypes = {
  search: PropTypes.func.isRequired,
  name: PropTypes.string,
};
const mapStateToProps=createStructuredSelector({
  name:searchName(),
})
const mapDispatchToProps = (dispatch)=>({
  search:(name)=>dispatch(search(name))
})
const withConnect=connect(
  mapStateToProps,mapDispatchToProps
);

export default compose(
  withReducer,
    withSaga,
    withConnect,
)(SearchResponent);
