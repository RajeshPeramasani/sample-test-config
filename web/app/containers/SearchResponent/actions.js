import {SEARCH} from './constants';

export const search=(name)=>({type:SEARCH,name});
