import {createSelector} from 'reselect';
import {initialState} from './reducer';


const selectState = state => state.get('login', initialState);


export const makeSelectUserName = () =>
  createSelector(selectState, substate => substate.username);

export const makeSelectPassword = () =>
  createSelector(selectState, substate => substate.password);
