/**
 *
 * Login
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Helmet} from 'react-helmet';
import {createStructuredSelector} from 'reselect';
import {compose} from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import {makeSelectUserName} from './selectors';
import {login} from './actions';
import reducer from './reducer';
import saga from './saga';
import './style.css';
import {Button, Input, Dimmer, Loader} from 'semantic-ui-react';


export class Login extends React.Component {
  state = {
    password: '',
    username: '',
    disableLoader: true
  };

  componentDidMount() {
    this.history = this.props.history;
  }

  onUserNameChange = (event, {value}) => {
    this.setState({username: value, usernameError: false});
  };

  onPasswordChange = (event, {value}) => {
    this.setState({password: value, passwordError: false});
  };

  validate() {
    const {password, username} = this.state;
    var usernameError = false;
    var passwordError = false;
    var error = false;

    if (!username || username.trim().length === 0 || !isNaN(username) || !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(username))) {
      error = usernameError = true;
    }
    if (!password || password.trim().length === 0 || password.length < 4) {
      error = passwordError = true;
    }

    this.setState({
      error,
      usernameError,
      passwordError,
    });
  }

  render() {
    const {username, password, disableLoader} = this.state;

    return (
      <div className='login'>
        <Helmet>
          <title>Login</title>
          <meta name="description" content="Description of Login"/>
        </Helmet>
        <h1>Login to continue</h1>
        <div style={{display: 'flex', flexDirection: 'column'}}>
          <Input placeholder='Email' onChange={this.onUserNameChange}/>
          <Input
            type='password'
            placeholder='Password'
            onChange={this.onPasswordChange}
          />
          <Button
            primary
            content='Login'
            onClick={() => this.props.login(username, password)}
          />
        </div>
        <Dimmer active inverted disabled={disableLoader} page={!disableLoader}>
          <Loader size='big' disabled={disableLoader} content="Authenticating User..."/>
        </Dimmer>
      </div>
    );
  }
}

Login.propTypes = {
  login: PropTypes.func.isRequired,
  username: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
  username: makeSelectUserName(),
});

const mapDispatchToProps = (dispatch) => ({
  login: (username, password) => dispatch(login(username, password))
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({key: 'login', reducer});
const withSaga = injectSaga({key: 'login', saga});

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Login);
