import {put, takeLatest} from 'redux-saga/effects';
import {LOGIN} from './constants';
import {loginSuccess, loginError} from './actions';
import request from '../../utils/request';
import {push} from 'react-router-redux';


export function* onLogin({username, password}) {
  try {
    const response = yield request({
      url: `${window.ajaxPrefix}/login`,
      type: 'POST',
      data: JSON.stringify({username, password})
    });

    yield put(loginSuccess(response));
    yield put(push('/user'));

  } catch (err) {
    yield console.log("Error  ", err);
    yield put(loginError(err));
  }
}


export default function* watchLogin() {
  // Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(LOGIN, onLogin);
}
