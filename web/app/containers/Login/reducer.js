/*
 *
 * Login reducer
 *
 */

import {fromJS} from 'immutable';
import {LOGIN, LOGIN_SUCCESS, LOGIN_ERROR} from './constants';

export const initialState = fromJS({});

function loginReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        username: action.username,
        password: action.password
      };
    case LOGIN_SUCCESS:
      console.log("SUCCESS")

      return {
        ...state,
        user: action.response
      };
    case LOGIN_ERROR:
      return state;
    default:
      return state;
  }
}

export default loginReducer;
