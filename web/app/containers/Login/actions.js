/*
 *
 * Login actions
 *
 */

import {LOGIN, LOGIN_ERROR, LOGIN_SUCCESS} from './constants';

export const login = (username, password) => ({type: LOGIN, username, password});
export const loginSuccess = (response) => ({type: LOGIN_SUCCESS, response});
export const loginError = (response) => ({type: LOGIN_ERROR, response});
